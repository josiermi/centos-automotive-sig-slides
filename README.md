# Centos Automotive SIG Slides

This is a simple repository of meeting slides from Automotive SIG meetings.

- [October 5, 2022 slides (pptx](https://gitlab.com/josiermi/centos-automotive-sig-slides/-/blob/main/CentOS_Automotive_SIG_-_October_5__2022.pptx)
